function [ output ] = calculateFrequencyOutputFileEmotiv( faible, intense, frisson )
% CALCULATEFREQUENCYOUTPUTFILEEMOTIV
    % This function calculate all valence and powervalue for each condition
    % input is 3 vector for each condition of the same bands
    % EX: faible(alpha) , intense(alpha) , frisson(alpha)
    
    % Output is a 1-row vector of 1 bands for 1 subject
    % Ex: V1_faible, V1_intense, V1_frisson, V1_1_faible V1_1_intense, V1_1_frisson
    
    all_condition = [faible;intense;frisson];
    [row, ~] = size(all_condition);
    
    output = {};
    V1 = {};
    V1_1 = {};
    V1_2 = {};
    V1_3 = {};
    V2 = {};
    V2_1 = {};
    V2_3 = {};
    PV4 = {};
    PV5 = {};
    PV6 = {};
    PV7 = {};
    PV8 = {};
    PV9 = {};
    PV10 = {};
    PV11 = {};
        
    for i=1:row 
        AF3 = all_condition(i,1);
        AF4 = all_condition(i,14);
        F3 = all_condition(i,3);
        F4 = all_condition(i,12);
        F7 = all_condition(i,2);
        F8 = all_condition(i,13);
        FC5 = all_condition(i,4);
        FC6 = all_condition(i,11);

        
        V1 = [V1 F4-F3];
    
        V1_1 = [V1_1 AF4-AF3];
    
        V1_2 = [V1_2 F8-F7];
        
        V1_3 = [V1_3 FC6-FC5];
        
        V2 = [V2 ((F4+AF4)/2)-((F3+AF3)/2)];
        
        V2_1 = [V2_1 ((F4+AF4+FC6)/3)-((F3+AF3+FC5)/3)];
        
        V2_3 = [V2_3 ((F4+AF4+FC6+F8)/4)-((F3+AF3+FC5+F7)/4)];

        PV4 = [PV4 F3];
        
        PV5 = [PV5 F4];
        
        PV6 = [PV6 AF3];
        
        PV7 = [PV7 AF4];
        
        PV8 = [PV8 FC5];
        
        PV9 = [PV9 FC6];
        
        PV10 = [PV10 F7];
        
        PV11 = [PV11 F8];
        
    end
    output = [output, V1,{''}, V1_1,{''}, V1_2,{''}, V1_3,{''}, V2,{''}, V2_1,{''}, V2_3,{''},...
        PV4,{''}, PV5,{''}, PV6,{''}, PV7,{''}, PV8,{''}, PV9,{''}, PV10,{''}, PV11];


end

