function [output ] = calculateFrequencyOutputFileGtec( faible, intense, frisson )
all_condition = [faible;intense;frisson];
    [row, ~] = size(all_condition);
    
    output = {};
    V1 = {};
    V1_1 = {};
    V1_2 = {};

    V2 = {};


    PV4 = {};
    PV5 = {};
    PV6 = {};
    PV7 = {};

    PV10 = {};
    PV11 = {};
        
    for i=1:row 
        % ('F8','F7','F3','F4','AF3','AF4','T7','T8')
        AF3 = all_condition(i,5);
        AF4 = all_condition(i,6);
        F3 = all_condition(i,3);
        F4 = all_condition(i,4);
        F7 = all_condition(i,2);
        F8 = all_condition(i,1);

        
        V1 = [V1 F4-F3];
    
        V1_1 = [V1_1 AF4-AF3];
    
        V1_2 = [V1_2 F8-F7];
        

        
        V2 = [V2 ((F4+AF4)/2)-((F3+AF3)/2)];
        


        PV4 = [PV4 F3];
        
        PV5 = [PV5 F4];
        
        PV6 = [PV6 AF3];
        
        PV7 = [PV7 AF4];
        

        
        PV10 = [PV10 F7];
        
        PV11 = [PV11 F8];
        
    end
    output = [output, V1,{''}, V1_1,{''}, V1_2,{''}, V2,{''},...
        PV4,{''}, PV5,{''}, PV6,{''}, PV7,{''}, PV10,{''}, PV11];




end

