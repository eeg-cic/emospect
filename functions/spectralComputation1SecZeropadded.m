function output = spectralComputation1SecZeropadded(epochlist,nelec,samplingrate,frequencytokeep)
% Give a matrix of PSD analysis 
% 1second and zeropadded into 4 sec
    n_topad = (samplingrate*3)/2;
    hanning_window = hann(samplingrate);
    output = [];
    % for each epoch
    for epoch = 1:length(epochlist)
        
        [~,~,~,signal] = openeph(epochlist{epoch});
        
        % for each electrode
        for electrode = 1:nelec
            signal_padded = padarray(signal(:,electrode),n_topad);
            [pxx1,~] = pwelch(signal_padded,hanning_window,samplingrate/2,samplingrate,samplingrate);
            eachepoch(1,:,electrode) = pxx1(1:frequencytokeep);
        end % for each electrode
        
        output = [output ; eachepoch];
   
    end % for each epoch
end
