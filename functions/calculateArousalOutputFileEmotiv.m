function output = calculateArousalOutputFileEmotiv(faible, intense, frisson )
    % CALCULATEAROUSALOUTPUTFILEEMOTIV
    % This function calculate all arousal for each condition
    % input is 3 vector for each condition of the same bands
    % EX: faible(alpha;beta) , intense(alpha;beta) , frisson(alpha;beta)
    
    all_condition = [faible;intense;frisson];
    [row, ~] = size(all_condition);

    output = {};
    V4 = {};
    A1 = {};
    A1_1 = {};
    
    for i = 1:2:row
        aAF3 = all_condition(i,1);
        aAF4 = all_condition(i,14);
        aF3 = all_condition(i,3);
        aF4 = all_condition(i,12);
        aF7 = all_condition(i,2);
        aF8 = all_condition(i,13);
        aFC5 = all_condition(i,4);
        aFC6 = all_condition(i,11);
        
        bAF3 = all_condition(i+1,1);
        bAF4 = all_condition(i+1,14);
        bF3 = all_condition(i+1,3);
        bF4 = all_condition(i+1,12);
        bF7 = all_condition(i+1,2);
        bF8 = all_condition(i+1,13);
        bFC5 = all_condition(i+1,4);
        bFC6 = all_condition(i+1,11);

        
        V4 = [V4 (aF4/bF4)-(aF3/bF3)];
        A1 = [A1 (bAF3+bAF4+bF3+bF4)/(aAF3+aAF4+aF3+aF4)];
        A1_1 = [A1_1 (bAF3+bAF4+bF3+bF4+bFC5+bFC6+bF7+bF8)/(aAF3+aAF4+aF3+aF4+aFC5+aFC6+aF7+aF8)];

    end
    output = [output, V4,{''}, A1,{''}, A1_1];
end

