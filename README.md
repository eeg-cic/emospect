# EmoSpect

## Input
The program will ask user to choose a folder. There are seperated main file for each devices, please make sure to choose accordingly.

Intput: 1 folder for each device with this structure.
  
`emotiv_data_folder`  
|  
|&ndash;&ndash;&ndash;&ndash; `AB01_subject_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `1_NeutreFaible_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch3.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `2_Intense_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch3.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `3_Frisson_folder`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch3.eph`  

## Statistic
The program for statistic calculation is in `main_stat.R`  
you can run the program and choose the output file from emospect.  
If the libraries are missing, they can be installed in R console (bottome console when open R-studio). 
###### The required libraries are 
- library('tidyverse')
- library('stringr')
- library('stats')
- library('lmerTest')
- library('psycho')
- library('openxlsx')  

For example: you can install `tidyverse` by 
1. Typing in the console of R studio `install.packages('tidyverse')`  
2. Hit enter
